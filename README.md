<b>KELOMPOK : GG03</b><br>
Sistem Informasi 2015<br>
Institut Teknologi Del<br><br>
Puji Napitupulu - 12S15003<br>
Safiah Sitorus - 12S15012<br>
Bonggal Siahaan - 12S15023<br>
Gatricia Gultom - 12S15056<br>

<b>EKTRAKSI FILE</b>

File dikompress dalam format .zip yang dipisahkan ke dalam beberapa bagian.
Proses yang dapat dilakukan untuk ekstraksi file adalah sebagai berikut:

1. Klik kanan pada file dengan tipe .zip (bukan file bertipe .z01, z02, dan lainnya)
2. Klik "extract here".
3. Tunggu sampai proses selesai.
4. Semua file telah selesai di-ekstrak dan dapat digunakan. (file dengan tipe .z01,
    .z02, dan lainnya tidak perlu di ekstrak)

<b>DESKRIPSI</b>

Project terdiri dari 2 direktori, yaitu:

1. movie_categorization_GUI, berisi file user interface dalam bentuk aplikasi web
    menggunakan framework DJANGO.
2. movie_categorization_ipynb_files, berisi file kode yang ditulis dalam bahasa python
    yang merupakan kode yang digunakan dalam klasifikasi film. kode tersebut juga
    digunakan dalam GUI.